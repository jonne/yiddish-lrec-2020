import helpers as h
import pandas as pd
import pickle
import sys
INPUT_PATH = '../data/meta/word_urls'
PICKLE_OUTPUT_PATH = '../data/raw/wiktionary_transliterations.pkl'
TXT_OUTPUT_PATH = '../data/raw/wiktionary_transliterations.txt'
CSV_OUTPUT_PATH = '../data/raw/wiktionary_transliterations.csv'

with open(INPUT_PATH, 'r') as f_in:
    word_urls = [url.strip() for url in f_in.readlines() if url.strip() != ""]

n = len(word_urls)
translit_dicts = []
for ix, url in enumerate(word_urls):
    print('{} / {}'.format(ix+1, n), url)
    try:
        result = h.fetch_transliteration(url)
        print(result)
        translit_dicts.append(result)
    except:
        print('Failed to fetch!')

yiddish_words = [d['yiddish'] for d in translit_dicts]
translit_words = [d['transliteration'] for d in translit_dicts]

output_format = input('What is the output format? (csv, pickle, txt) > ')

if output_format == 'pickle':
    with open(PICKLE_OUTPUT_PATH, 'wb') as f_out:
        pickle.dump(translit_dicts, f_out)

elif output_format == 'txt':
    with open(TXT_OUTPUT_PATH, 'a') as f_out:
        for yiddish_word, transliteration in zip(yiddish_words, translit_words):
            f_out.write('{}\t{}'.format(yiddish_word, transliteration))

else:
    out = pd.DataFrame()
    out['yiddish'] = yiddish_words
    out['transliteration'] = translit_words
    if output_format == 'csv':
        out.to_csv(CSV_OUTPUT_PATH, index=False, encoding='utf-8')
    else:
        print(out)
        sys.exit(0)
