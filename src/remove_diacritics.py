from transformers import *
import pandas as pd
import itertools as it

INPUT_PATH = '../data/wiktionary_standardized.csv'
OUTPUT_PATH = '../data/wiktionary_standardized_nodiacritics.csv'

token_transformer = TokenTransformer()
corpus_transformer = CorpusTransformer(token_transformer)

corpus = pd.read_csv(INPUT_PATH)
no_diacritics = corpus_transformer.remove_diacritics(corpus, return_single_col=True)
corpus['yiddish_without_diacritics'] = no_diacritics

corpus.to_csv(OUTPUT_PATH, index=False, encoding='utf-8')
