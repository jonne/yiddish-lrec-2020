שריפֿטשטעלער	s h r i f t s h t e l e r
אַנטלויף	a n t l o y f
באַראָן	b a r o n
אַכטל	a k h t l
עלטערן	e l t e r n
ברוסט	b r u s t
ניהיליזם	n i h i l i z m
גרונט	g r u n t
װײַלע	v a y l e
בהמה	b e h e y m e
קאַרשנבוים	k a r s h n b o y m
באַל	b a l
בעל־הביתטע	b a l e b o s t e
זשיװעצע	z h i v e t s e
פּאַלעאָנטאָלאָג	p a l e o n t o l o g
יאַגדע	y a g d e
קאָפּ	k o p
קלײ	k l e y
אַקטאָר	a k t o r
דרוקערײַ	d r u k e r a y
פּלאַזשע	p l a z h e
באָלשעװיסט	b o l s h e v i s t
פּידקאָװע	p i d k o v e
דרוקער	d r u k e r
חזן	k h a z n
האָב	h o b
רביסטװע	r e b i s t v e
צײַטשריפֿט	t s a y t s h r i f t
קאָסמאָנויט	k o s m o n o y t
יתומל	y o s e m l
קרויט	k r o y t
ליטװאַק	l i t v a k
זינגערקע	z i n g e r k e
מאָדערניזם	m o d e r n i z m
אַפּאַראַטשיק	a p a r a t s h i k
מילה	m i l e
גלײַכונג	g l a y k h u n g
עמך	a m k h o
אַנטיסעמיט	a n t i s e m i t
בלי	b l i
נול	n u l
זינגליד	z i n g l i d
גבור	g i b e r
פֿוקס	f u k s
קריג	k r i g
נעגערטע	n e g e r t e
גערשט	g e r s h t
קין	k i n
אַחרון	a k h r e n
מלאכה	m e l o k h e
קלויסטער	k l o y s t e r
רעצעפּט	r e t s e p t
װיגל	v i g l
גאָון	g o e n
פֿאָטערלאַנד	f o t e r l a n d
װערטעלע	v e r t e l e
ספּאָדיק	s p o d i k
אוראַניום	u r a n y u m
װאַנילע	v a n i l e
בעבל	b e b l
קלאָג	k l o g
דאַנקבאַרקײט	d a n k b a r k e y t
בײגל	b e y g l
סאַראָקע	s a r o k e
אָקופּאַציע	o k u p a t s y e
לבֿנה	l e v o n e
פּושקע	p u s h k e
אַרץ	a r t s
פּאַפּיראָס	p a p i r o s
גולאַש	g u l a s h
דאָרף	d o r f
װײן	v a y n
מאָנאָפּאָל	m o n o p o l
תּל	t e l
זומערקורס	z u m e r k u r s
פּראָיעקט	p r o y e k t
באָלשעװיק	b o l s h e v i k
הײַסקול	h a y s k u l
אָבֿ	o v
קאַװע	k a v e
צאַפּנבערדל	t s a p n b e r d l
געבוירן־טאָג	g e b o y r n - t o g
אָרון	o r n
קעמפֿער	k e m f e r
שרײַבערין	s h r a y b e r i n
קאָשיקל	k o s h i k l
װײַסרוסיש	v a y s r u s i s h
קאַרפּ	k a r p
חבֿרטע	k h a v e r t e
האָז	h o z
העלפֿט	h e l f t
לענד	l e n d
אַרבעטער	a r b e t e r
גלאָבוס	g l o b u s
באַפֿרײַער	b a f r a y e r
מאָדערניסט	m o d e r n i s t
פּיראָג	p i r o g
שפּינװעב	s h p i n v e b
טראַף	t r a f
טעפּער	t e p e r
נוץ	n u t s
ראשון	r i s h e n
לעפּל	l e p l
פֿאַראַכטונג	f a r a k h t u n g
אַפּאָסטאָל	a p o s t o l
עפּידעמיאָלאָגיע	e p i d e m y o l o g y e
צירונג	t s i r u n g
טיפֿלע	t i f l e
צימרינג	t s i m r i n g
װעלטײל	v e l t e y l
ביבליאָטעק	b i b l y o t e k
רעפּעטיציע	r e p e t i t s y e
קאָפּעקע	k o p e k e
פֿערשקע	f e r s h k e
װאַמפּיר	v a m p i r
תּחת	t o k h e s
ליבשאַפֿט	l i b s h a f t
קאַװענע	k a v e n e
קערניצע	k e r n i t s e
מחותּנישאַפֿט	m e k h u t o n i s h a f t
קערן	k e r n
להכעיס	l e h a k h e s
שמירקעז	s h m i r k e z
באָלשעװיזם	b o l s h e v i z m
קאָנװערב	k o n v e r b
כּעס	k a s
פּאָליאַטשקע	p o l y a t s h k e
װעש	v e s h
ציטער	t s i t e r
שילדערונג	s h i l d e r u n g
אַנסאַמבל	a n s a m b l
פֿויגל	f o y g l
הצלחה	h a t s l o k h e
לײַב	l a y b
אַרכעאָלאָגיע	a r k h e o l o g y e
פּנימל	p e n e m l
שמעקעלע	s h m e k e l e
אײר	i y e r
תּבֿואה	t v u e
צענטער	t s e n t e r
עקרה	a k o r e
מאַהאַראַדזשאַ	m a h a r a d z h a
שתוהל	s h t u h l
סאָציאָלאָגיע	s o t s y o l o g y e
װערטערבוך	v e r t e r b u k h
עפּילאָג	e p i l o g
פּאָעזיע	p o e z y e
סעקס	s e k s
אור־באָבע	u r - b o b e
פּינגװין	p i n g v i n
אָדערל	o d e r l
אַשבוים	a s h b o y m
רחמנות	r a k h m o n e s
װעט	v e t
אײגנטום	e y g n t u m
פּרשה	p a r s h e
אויטאָנאָמיע	o y t o n o m y e
שאָך	s h o k h
ספֿר־תּורה	s e y f e r - t o y r e
אַנטשולדיקונג	a n t s h u l d i k u n g
עוף	o f
שנאָז	s h n o z
הארץ	h a r t s
זיבעטל	z i b e t l
פֿיסטאַשקע	f i s t a s h k e
שטודיע	s h t u d y e
ראָמאַן	r o m a n
געאָמעטריע	g e o m e t r y e
אויטאָביאָגראַפֿיע	o y t o b i o g r a f y e
פֿערישקע	f e r i s h k e
בעכער	b e k h e r
מאָגן	m o g n
ביצה	b e y t s e
חרם	k h e y r e m
טעלעגראַם	t e l e g r a m
פֿאַזאַן	f a z a n
כּלל	k l a l
זײַט	z a y t
כראַבינע	k h r a b i n e
ביוראָקראַט	b y u r o k r a t
עולם	o y l e m
דאַך	d a k h
אחד	e k h o d
קעמל	k e m l
עצה	e y t s e
פּאָרפֿאָלק	p o r f o l k
נאַצי	n a t s i
פּלאַנעט	p l a n e t
כראָנאָלאָגיע	k h r o n o l o g y e
בעריאָזע	b e r y o z e
אָרט	o r t
באַלן	b a l n
פּויפּסט	p o y p s t
ציגעלע	t s i g e l e
חסיד	k h o s i d
ברענהאָלץ	b r e n h o l t s
עפּלבוים	e p l b o y m
לידערבוך	l i d e r b u k h
טעלער	t e l e r
מזרח	m i z r e k h
עליה	a l i e
אײַז	a y z
ביליאָן	b i l y o n
מזל	m a z l
מערבֿ	m a y r e v
קירזשנער	k i r z h n e r
אינהאַלט	i n h a l t
אונטערשיד	u n t e r s h i d
תּשרי	t i s h r e
תּחת לעקער	t o k h e s - l e k e r
באָבע	b o b e
איבערמענטש	i b e r m e n t s h
אײַזנגעשעפֿט	a y z n g e s h e f t
תּירוץ	t e y r e t s
װוינצימער	v o y n t s i m e r
פֿונט	f u n t
שידוך	s h i d e k h
עומר	o y m e r
אײ	e y
שײן	s h e y n
װאָגן	v o g n
אַפּטײק	a p t e y k
נדן	n a d n
צאָן	t s o n
עטנאָגראַפֿיע	e t n o g r a f y e
פּאַיאַץ	p a y a t s
פֿוטער	f u t e r
אַדאַפּטירער	a d a p t i r e r
װײַנשל	v a y n s h l
נקמה	n e k o m e
כּלי־זײן	k l e z a y e n
ביציקל	b i t s i k l
עקזעמפּלאַר	e k z e m p l a r
פֿאָנעם	f o n e m
שמאַלץ	s h m a l t s
האַװן	h a v n
געאָלאָג	g e o l o g
מעטער	m e t e r
אַנולירונג	a n u l i r u n g
פֿלײש	f l e y s h
אַרכיװ	a r k h i v
פּאָטקאָװע	p o t k o v e
גרויסקײט	g r o y s k e y t
בגד	b e g e d
גענעראַל	g e n e r a l
ראַבינער	r a b i n e r
רוגזה	r u g z e
שלימזל	s h l i m a z l
רעדאַקטאָר	r e d a k t o r
כירורג	k h i r u r g
מינדערהײט	m i n d e r h e y t
הלכה	h a l o k h e
מאַנגאַן	m a n g a n
שיכּור	s h i k e r
בליץ	b l i t s
אָנקל	o n k l
חסידיזם	k h s i d i z m
צײַטונג	t s a y t u n g
אַסטראָנויט	a s t r o n o y t
זשאַרגאָן	z h a r g o n
לײַבקע	l a y b k e
אַגרעס	a g r e s
בויטשיק	b o y t s h i k
רוקן	r u k n
לעקערל	l e k e r l
האַס	h a s
חײל	k h a y e l
נאַגאַן	n a g a n
שאָף	s h o f
דעמב	d e m b
גאַל	g a l
ביכערקראָם	b i k h e r k r o m
שמיד	s h m i d
אַנטראָפּאָלאָגיע	a n t r o p o l o g y e
האָר	h o r