shriftshteler	ש ר י פ ֿ ט ש ט ע ל ע ר
antloyf	א ַ נ ט ל ו י ף
baron	ב א ַ ר א ָ ן
akhtl	א ַ כ ט ל
eltern	ע ל ט ע ר ן
brust	ב ר ו ס ט
nihilizm	נ י ה י ל י ז ם
grunt	ג ר ו נ ט
vayle	װ ײ ַ ל ע
beheyme	ב ה מ ה
karshnboym	ק א ַ ר ש נ ב ו י ם
bal	ב א ַ ל
baleboste	ב ע ל ־ ה ב י ת ט ע
zhivetse	ז ש י װ ע צ ע
paleontolog	פ ּ א ַ ל ע א ָ נ ט א ָ ל א ָ ג
yagde	י א ַ ג ד ע
kop	ק א ָ פ ּ
kley	ק ל ײ
aktor	א ַ ק ט א ָ ר
drukeray	ד ר ו ק ע ר ײ ַ
plazhe	פ ּ ל א ַ ז ש ע
bolshevist	ב א ָ ל ש ע װ י ס ט
pidkove	פ ּ י ד ק א ָ װ ע
druker	ד ר ו ק ע ר
khazn	ח ז ן
hob	ה א ָ ב
rebistve	ר ב י ס ט װ ע
tsaytshrift	צ ײ ַ ט ש ר י פ ֿ ט
kosmonoyt	ק א ָ ס מ א ָ נ ו י ט
yoseml	י ת ו מ ל
kroyt	ק ר ו י ט
litvak	ל י ט װ א ַ ק
zingerke	ז י נ ג ע ר ק ע
modernizm	מ א ָ ד ע ר נ י ז ם
aparatshik	א ַ פ ּ א ַ ר א ַ ט ש י ק
mile	מ י ל ה
glaykhung	ג ל ײ ַ כ ו נ ג
amkho	ע מ ך
antisemit	א ַ נ ט י ס ע מ י ט
bli	ב ל י
nul	נ ו ל
zinglid	ז י נ ג ל י ד
giber	ג ב ו ר
fuks	פ ֿ ו ק ס
krig	ק ר י ג
negerte	נ ע ג ע ר ט ע
gersht	ג ע ר ש ט
kin	ק י ן
akhren	א ַ ח ר ו ן
melokhe	מ ל א כ ה
kloyster	ק ל ו י ס ט ע ר
retsept	ר ע צ ע פ ּ ט
vigl	װ י ג ל
goen	ג א ָ ו ן
foterland	פ ֿ א ָ ט ע ר ל א ַ נ ד
vertele	װ ע ר ט ע ל ע
spodik	ס פ ּ א ָ ד י ק
uranyum	א ו ר א ַ נ י ו ם
vanile	װ א ַ נ י ל ע
bebl	ב ע ב ל
klog	ק ל א ָ ג
dankbarkeyt	ד א ַ נ ק ב א ַ ר ק ײ ט
beygl	ב ײ ג ל
saroke	ס א ַ ר א ָ ק ע
okupatsye	א ָ ק ו פ ּ א ַ צ י ע
levone	ל ב ֿ נ ה
pushke	פ ּ ו ש ק ע
arts	א ַ ר ץ
papiros	פ ּ א ַ פ ּ י ר א ָ ס
gulash	ג ו ל א ַ ש
dorf	ד א ָ ר ף
vayn	װ ײ ן
monopol	מ א ָ נ א ָ פ ּ א ָ ל
tel	ת ּ ל
zumerkurs	ז ו מ ע ר ק ו ר ס
proyekt	פ ּ ר א ָ י ע ק ט
bolshevik	ב א ָ ל ש ע װ י ק
hayskul	ה ײ ַ ס ק ו ל
ov	א ָ ב ֿ
kave	ק א ַ װ ע
tsapnberdl	צ א ַ פ ּ נ ב ע ר ד ל
geboyrn-tog	ג ע ב ו י ר ן ־ ט א ָ ג
orn	א ָ ר ו ן
kemfer	ק ע מ פ ֿ ע ר
shrayberin	ש ר ײ ַ ב ע ר י ן
koshikl	ק א ָ ש י ק ל
vaysrusish	װ ײ ַ ס ר ו ס י ש
karp	ק א ַ ר פ ּ
khaverte	ח ב ֿ ר ט ע
hoz	ה א ָ ז
helft	ה ע ל פ ֿ ט
lend	ל ע נ ד
arbeter	א ַ ר ב ע ט ע ר
globus	ג ל א ָ ב ו ס
bafrayer	ב א ַ פ ֿ ר ײ ַ ע ר
modernist	מ א ָ ד ע ר נ י ס ט
pirog	פ ּ י ר א ָ ג
shpinveb	ש פ ּ י נ װ ע ב
traf	ט ר א ַ ף
teper	ט ע פ ּ ע ר
nuts	נ ו ץ
rishen	ר א ש ו ן
lepl	ל ע פ ּ ל
farakhtung	פ ֿ א ַ ר א ַ כ ט ו נ ג
apostol	א ַ פ ּ א ָ ס ט א ָ ל
epidemyologye	ע פ ּ י ד ע מ י א ָ ל א ָ ג י ע
tsirung	צ י ר ו נ ג
tifle	ט י פ ֿ ל ע
tsimring	צ י מ ר י נ ג
velteyl	װ ע ל ט ײ ל
biblyotek	ב י ב ל י א ָ ט ע ק
repetitsye	ר ע פ ּ ע ט י צ י ע
kopeke	ק א ָ פ ּ ע ק ע
fershke	פ ֿ ע ר ש ק ע
vampir	װ א ַ מ פ ּ י ר
tokhes	ת ּ ח ת
libshaft	ל י ב ש א ַ פ ֿ ט
kavene	ק א ַ װ ע נ ע
kernitse	ק ע ר נ י צ ע
mekhutonishaft	מ ח ו ת ּ נ י ש א ַ פ ֿ ט
kern	ק ע ר ן
lehakhes	ל ה כ ע י ס
shmirkez	ש מ י ר ק ע ז
bolshevizm	ב א ָ ל ש ע װ י ז ם
konverb	ק א ָ נ װ ע ר ב
kas	כ ּ ע ס
polyatshke	פ ּ א ָ ל י א ַ ט ש ק ע
vesh	װ ע ש
tsiter	צ י ט ע ר
shilderung	ש י ל ד ע ר ו נ ג
ansambl	א ַ נ ס א ַ מ ב ל
foygl	פ ֿ ו י ג ל
hatslokhe	ה צ ל ח ה
layb	ל ײ ַ ב
arkheologye	א ַ ר כ ע א ָ ל א ָ ג י ע
peneml	פ ּ נ י מ ל
shmekele	ש מ ע ק ע ל ע
iyer	א ײ ר
tvue	ת ּ ב ֿ ו א ה
tsenter	צ ע נ ט ע ר
akore	ע ק ר ה
maharadzha	מ א ַ ה א ַ ר א ַ ד ז ש א ַ
shtuhl	ש ת ו ה ל
sotsyologye	ס א ָ צ י א ָ ל א ָ ג י ע
verterbukh	װ ע ר ט ע ר ב ו ך
epilog	ע פ ּ י ל א ָ ג
poezye	פ ּ א ָ ע ז י ע
seks	ס ע ק ס
ur-bobe	א ו ר ־ ב א ָ ב ע
pingvin	פ ּ י נ ג װ י ן
oderl	א ָ ד ע ר ל
ashboym	א ַ ש ב ו י ם
rakhmones	ר ח מ נ ו ת
vet	װ ע ט
eygntum	א ײ ג נ ט ו ם
parshe	פ ּ ר ש ה
oytonomye	א ו י ט א ָ נ א ָ מ י ע
shokh	ש א ָ ך
seyfer-toyre	ס פ ֿ ר ־ ת ּ ו ר ה
antshuldikung	א ַ נ ט ש ו ל ד י ק ו נ ג
of	ע ו ף
shnoz	ש נ א ָ ז
harts	ה א ר ץ
zibetl	ז י ב ע ט ל
fistashke	פ ֿ י ס ט א ַ ש ק ע
shtudye	ש ט ו ד י ע
roman	ר א ָ מ א ַ ן
geometrye	ג ע א ָ מ ע ט ר י ע
oytobiografye	א ו י ט א ָ ב י א ָ ג ר א ַ פ ֿ י ע
ferishke	פ ֿ ע ר י ש ק ע
bekher	ב ע כ ע ר
mogn	מ א ָ ג ן
beytse	ב י צ ה
kheyrem	ח ר ם
telegram	ט ע ל ע ג ר א ַ ם
fazan	פ ֿ א ַ ז א ַ ן
klal	כ ּ ל ל
zayt	ז ײ ַ ט
khrabine	כ ר א ַ ב י נ ע
byurokrat	ב י ו ר א ָ ק ר א ַ ט
oylem	ע ו ל ם
dakh	ד א ַ ך
ekhod	א ח ד
keml	ק ע מ ל
eytse	ע צ ה
porfolk	פ ּ א ָ ר פ ֿ א ָ ל ק
natsi	נ א ַ צ י
planet	פ ּ ל א ַ נ ע ט
khronologye	כ ר א ָ נ א ָ ל א ָ ג י ע
beryoze	ב ע ר י א ָ ז ע
ort	א ָ ר ט
baln	ב א ַ ל ן
poypst	פ ּ ו י פ ּ ס ט
tsigele	צ י ג ע ל ע
khosid	ח ס י ד
brenholts	ב ר ע נ ה א ָ ל ץ
eplboym	ע פ ּ ל ב ו י ם
liderbukh	ל י ד ע ר ב ו ך
teler	ט ע ל ע ר
mizrekh	מ ז ר ח
alie	ע ל י ה
ayz	א ײ ַ ז
bilyon	ב י ל י א ָ ן
mazl	מ ז ל
mayrev	מ ע ר ב ֿ
kirzhner	ק י ר ז ש נ ע ר
inhalt	א י נ ה א ַ ל ט
untershid	א ו נ ט ע ר ש י ד
tishre	ת ּ ש ר י
tokhes-leker	ת ּ ח ת   ל ע ק ע ר
bobe	ב א ָ ב ע
ibermentsh	א י ב ע ר מ ע נ ט ש
ayzngesheft	א ײ ַ ז נ ג ע ש ע פ ֿ ט
teyrets	ת ּ י ר ו ץ
voyntsimer	װ ו י נ צ י מ ע ר
funt	פ ֿ ו נ ט
shidekh	ש י ד ו ך
oymer	ע ו מ ר
ey	א ײ
sheyn	ש ײ ן
vogn	װ א ָ ג ן
apteyk	א ַ פ ּ ט ײ ק
nadn	נ ד ן
tson	צ א ָ ן
etnografye	ע ט נ א ָ ג ר א ַ פ ֿ י ע
payats	פ ּ א ַ י א ַ ץ
futer	פ ֿ ו ט ע ר
adaptirer	א ַ ד א ַ פ ּ ט י ר ע ר
vaynshl	װ ײ ַ נ ש ל
nekome	נ ק מ ה
klezayen	כ ּ ל י ־ ז ײ ן
bitsikl	ב י צ י ק ל
ekzemplar	ע ק ז ע מ פ ּ ל א ַ ר
fonem	פ ֿ א ָ נ ע ם
shmalts	ש מ א ַ ל ץ
havn	ה א ַ װ ן
geolog	ג ע א ָ ל א ָ ג
meter	מ ע ט ע ר
anulirung	א ַ נ ו ל י ר ו נ ג
fleysh	פ ֿ ל ײ ש
arkhiv	א ַ ר כ י װ
potkove	פ ּ א ָ ט ק א ָ װ ע
groyskeyt	ג ר ו י ס ק ײ ט
beged	ב ג ד
general	ג ע נ ע ר א ַ ל
rabiner	ר א ַ ב י נ ע ר
rugze	ר ו ג ז ה
shlimazl	ש ל י מ ז ל
redaktor	ר ע ד א ַ ק ט א ָ ר
khirurg	כ י ר ו ר ג
minderheyt	מ י נ ד ע ר ה ײ ט
halokhe	ה ל כ ה
mangan	מ א ַ נ ג א ַ ן
shiker	ש י כ ּ ו ר
blits	ב ל י ץ
onkl	א ָ נ ק ל
khsidizm	ח ס י ד י ז ם
tsaytung	צ ײ ַ ט ו נ ג
astronoyt	א ַ ס ט ר א ָ נ ו י ט
zhargon	ז ש א ַ ר ג א ָ ן
laybke	ל ײ ַ ב ק ע
agres	א ַ ג ר ע ס
boytshik	ב ו י ט ש י ק
rukn	ר ו ק ן
lekerl	ל ע ק ע ר ל
has	ה א ַ ס
khayel	ח ײ ל
nagan	נ א ַ ג א ַ ן
shof	ש א ָ ף
demb	ד ע מ ב
gal	ג א ַ ל
bikherkrom	ב י כ ע ר ק ר א ָ ם
shmid	ש מ י ד
antropologye	א ַ נ ט ר א ָ פ ּ א ָ ל א ָ ג י ע
hor	ה א ָ ר