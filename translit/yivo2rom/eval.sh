# Evaluates the model on the test set
TRAIN_PATH="../../data/lex/wiktionary_yivo2rom_train.lex"
TEST_PATH="../../data/lex/wiktionary_yivo2rom_test.lex"

echo "Train set metrics"
g2p.py -e UTF-8 --model model-6 --test $TRAIN_PATH | tail -11

echo "Test set metrics"
g2p.py -e UTF-8 --model model-6 --test $TEST_PATH | tail -11
